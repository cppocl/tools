#!/usr/bin/env python

# Copyright 2020 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys

## Split strin with extensions into a list, removing '*'.
#  E.g. "*.c;*.cpp" becomes [".c", ".cpp"]
def split_file_types(search):
    tmp = search.split(";")
    extensions = []
    for ext in tmp:
        if ext[0] == '*':
            ext = ext[1:]
        extensions.append(ext)
    return extensions

## Find files matching the file extensions.
def find_source_files(path, extensions):
    found = []

    for root, dirs, files in os.walk(path):
        for f in files:
            for ext in extensions:
                if f.endswith(ext):
                    file_path = os.path.join(root, f)
                    if root[0] == ".": # Remove ".\\" or "./"
                        file_path = file_path[2:]
                    found.append(file_path.replace("\\", "/"))
                    break

    return found

## Replace file extension matched in list of extensions with extension.
def replace_extension(filename, extensions, extension):
    for e in extensions:
        if filename.endswith(e):
            filename = filename[:-len(e)] + extension
            break
    return filename

## Write a file containing the lines in the list.
def create_file(filename, lines):
    try:
        f = open(filename, "w+")
    except:
        sys.stderr.write("Unable to create file: {}\n".format(filename))
        return False
    for line in lines:
        f.write("{}\n".format(line))
    f.close()

    return True

## Create a common makefile for cppocl projects.
def create_makefile(filename, files, extensions, exe_name):

    obj_files = []

    lines = [
        "# compiler and flags.",
        "CC=g++",
        "CFLAGS=-Wall",
        "",
        "# folders.",
        "OBJDIR=obj",
        "EXEDIR=bin",
        "",
        "# target executable.",
        "EXENAME={}".format(exe_name),
        ""
    ]

    # Move exe name to top of file list.
    pos = 0
    for f in files:
        if replace_extension(f, extensions, "") == exe_name:
            break
        pos += 1

    if pos == len(files):
        sys.stderr.write("exe name not found in list of files.\n")
        return False

    exe_name_with_extension = files[pos]
    del files[pos]
    files.insert(0, exe_name_with_extension)

    # Add object (.o) files.
    leading = "OBJECTS=$(OBJDIR)/"
    for f in files:
        lines.append(leading + replace_extension(f, extensions, ".o \\"))
        leading = "        $(OBJDIR)/"

    if len(lines) > 0:
        lines[len(lines)-1] = lines[len(lines)-1][:-2]

    lines.append("")
    lines.append("# object files and executable targets.")
    lines.append("all : $(OBJECTS) $(EXEDIR)/$(EXENAME)")
    lines.append("")
    lines.append("# pre-requisite for targets.")
    lines.append("$(OBJECTS) : | $(OBJDIR)")
    lines.append("")
    lines.append("$(OBJDIR):")
    lines.append("\tmkdir -p $(OBJDIR) $(EXEDIR)")
    lines.append("")
    lines.append("# build target.")
    lines.append("$(EXEDIR)/$(EXENAME) : $(OBJECTS)")
    lines.append("\t$(CC) $(CFLAGS) -o $(EXEDIR)/$(EXENAME) $(OBJECTS)")
    lines.append("")
    lines.append("# build objects.")
    lines.append("$(OBJDIR)/$(EXENAME).o : $(EXENAME).cpp")
    lines.append("\t$(CC) $(CFLAGS) -c $(EXENAME).cpp -o $(OBJDIR)/$(EXENAME).o")

    # Add all remaining files to the list of objects to be built.
    del files[0]
    for f in files:
        obj_name = f + ".o"
        cpp_name = f + ".cpp"
        lines.append("$(OBJDIR)/{} : ".format(obj_name, cpp_name))
        lines.append("\t$(CC) $(CFLAGS) -c {} -o $(OBJDIR)/{}".format(cpp_name, obj_name))

    lines.append("")
    lines.append("# clean files. (delete object files and executable)")
    lines.append(".PHONY : clean")
    lines.append("clean :")
    lines.append("\trm $(EXEDIR)/$(EXENAME) $(OBJECTS)")
    lines.append("\trmdir $(EXEDIR)")
    lines.append("\trmdir $(OBJDIR)")

    return create_file(filename, lines)

if __name__ == "__main__":

    if len(sys.argv) == 4:
        folder   = sys.argv[1]
        search   = sys.argv[2]
        exe_name = sys.argv[3]
        extensions = split_file_types(search)
        files = find_source_files(folder, extensions)
        if not create_makefile("makefile", files, extensions, exe_name):
            sys.stderr.write("Unable to create makefile\n")
    else:
        print("Unexpected arguments.\n\n"
              "create_makefile.py <folder> <search> <exe name>\n\n"
              "folder   - A sub-directory relative to the current path.\n"
              "search   - A list of file extensions to compile, separated by a semi-colon.\n"
              "exe name - Name of a .cpp file without the extension,"
              "           used for compiling and output executable file.\n"
              "e.g.\n"
              "    create_makefile.py myapp *.c;*.cpp myapp_unit_tests")
