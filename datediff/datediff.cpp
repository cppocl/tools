/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../datetime/Date.hpp"
#include "../../datetime/NowDate.hpp"
#include "../print_version.hpp"
#include <cstdio>
#include <cstring>
#include <cstddef>
#include <cstdlib>
#include <cctype>

// Return status from function main.
const int STATUS_OK = 0;
const int STATUS_HELP = 1;
const int STATUS_SYNTAX_ERROR = 2;

using ocl::Date;
using ocl::NowDate;

#define RELEASE_DATE "2018/12/28 22:00"
#define PREVIOUSLY_UPDATED "2018/04/12 17:00"

void PrintHelp()
{
    printf("Syntax:\n"
           "    datediff <start date> <end date> [--verbose]\n"
           "                                     [-v | --version]\n"
           "                                     [-v=full | --version=full]\n"
           "                                     [-v=simple | --version=simple]\n"
           "                                     [-h | --help]\n\n"
           "Information:\n"
           "    Date is in the format of YYYY-MM-DD\n"
           "    Date can also be now, which will use todays year, month and day\n"
           "    If the date is now, then now must be lower case.\n"
           "    The number returned is the difference in days or "
           "days, months and years when specifying --verbose.\n\n"
           "e.g.\n"
           "    datediff 2018-01-15 2018-03-05\n"
           "    datediff 1980-11-02 now\n"
           "    datediff --verbose 2018-01-15 2018-03-05\n");
}

std::size_t CopyDigits(char* dest, char const* src, std::size_t count)
{
    std::size_t pos = 0;
    for (; pos < count; ++pos)
    {
        char ch = src[pos];
        if (!isdigit(ch))
            break;
        dest[pos] = ch;
    }
    dest[pos] = '\0';
    return pos;
}

bool StringToDate(const char* str, Date& date, char& sep)
{
    const std::size_t YEAR_DIGITS = 4;
    const std::size_t MONTH_DIGITS = 2;
    const std::size_t DAY_DIGITS = 2;

    char year[YEAR_DIGITS + 1];
    char month[MONTH_DIGITS + 1];
    char day[DAY_DIGITS + 1];

    bool success = (str != nullptr);

    // Parse year part of string.
    if (sep == '\0')
        sep = str[YEAR_DIGITS];
    if (sep == '-' || sep == '/')
    {
        success = (CopyDigits(year, str, YEAR_DIGITS) == YEAR_DIGITS) && (str[YEAR_DIGITS] == sep);
        if (success)
            str += YEAR_DIGITS + 1;

        // Parse month part of string.
        success = success && (CopyDigits(month, str, MONTH_DIGITS) == MONTH_DIGITS) && (str[MONTH_DIGITS] == sep);
        if (success)
            str += MONTH_DIGITS + 1;

        // Parse day part of string.
        success = success && (CopyDigits(day, str, DAY_DIGITS) == DAY_DIGITS) && (str[DAY_DIGITS] == '\0');

        if (success)
        {
            uint16_t y = static_cast<uint16_t>(::strtoul(year, nullptr, 10));
            uint8_t  m = static_cast<uint8_t>(::strtoul(month, nullptr, 10));
            uint8_t  d = static_cast<uint8_t>(::strtoul(day, nullptr, 10));
            success = (Date::IsValidDate(d, m, y));
            if (success)
                date.SetDate(d, m, y);
            else
                date.SetDate(1, Date::JANUARY, 1900);
        }
    }
    else
        success = false;

    return success;
}

void PrintDays(Date const& first, Date const& second)
{
    unsigned long days;
    if (first < second)
        days = static_cast<unsigned long>(first.GetDifferenceInDays(second));
    else
        days = static_cast<unsigned long>(second.GetDifferenceInDays(first));
    printf("%lu days\n", days);
}

void PrintDaysMonthsAndYears(Date const& first, Date const& second)
{
    Date::day_type days;
    Date::month_type months;
    Date::year_type years;
    if (first < second)
        first.GetDifference(second, days, months, years);
    else
        second.GetDifference(first, days, months, years);
    const char* sep = "";
    if (days > 0)
    {
        char const* days_str = days > 1 ? "days" : "day";
        printf("%u %s", static_cast<unsigned int>(days), days_str);
        sep = (months > 0) && (years > 0) ? ", " : " and ";
    }
    if (months > 0)
    {
        char const* months_str = months > 1 ? "months" : "month";
        printf("%s%u %s", sep, static_cast<unsigned int>(months), months_str);
        sep = " and ";
    }
    if (years > 0)
    {
        char const* years_str = years > 1 ? "years" : "year";
        printf("%s%u %s", sep, static_cast<unsigned int>(years), years_str);
    }
    else if (*sep == '\0')
        printf("0 days");
    printf("\n");
}

bool GetDates(int argc, char* argv[], Date* dates, bool& is_verbose, int& error_pos)
{
    bool date_valid = true;

    // Either two dates, or two dates and --verbose are allowed.
    bool find_verbose = argc == 4;

    // Due to an argument possibly being --verbose, need to keep track of date.
    int date_pos = 0;

    // Separator can be '-' or '/'. '\0' means not yet identified.
    char sep = '\0';

    for (int arg = 1; arg < argc && date_valid; ++arg)
    {
        char* arg_str = argv[arg];
        if (find_verbose && ::strcmp(arg_str, "--verbose") == 0)
            is_verbose = true;
        else
        {
            if (date_pos < 2)
            {
                Date& date = dates[date_pos];
                if (::strcmp(arg_str, "now") == 0)
                    date = NowDate::Now();
                else if (!StringToDate(arg_str, date, sep))
                {
                    date_valid = false;
                    break;
                }
                else
                    ++date_pos;
            }
            else
                date_valid = false;
        }
    }
    if (!date_valid)
        error_pos = date_pos;

    return date_valid;
}

int main(int argc, char* argv[])
{
    int status;

    if ((argc != 3) && (argc != 4))
    {
        if (argc == 2)
        {
            if (::strcmp(argv[1], "-h") == 0 || ::strcmp(argv[1], "--help") == 0)
                PrintHelp();
            else if (::strcmp(argv[1], "-v") == 0 ||
                     ::strcmp(argv[1], "--version") == 0 ||
                     ::strcmp(argv[1], "-v=full") == 0 ||
                     ::strcmp(argv[1], "--version=full") == 0)
            {
                ocl::PrintVersion(RELEASE_DATE, PREVIOUSLY_UPDATED);
            }
            else if (::strcmp(argv[1], "-v=simple") == 0 ||
                     ::strcmp(argv[1], "--version=simple") == 0)
            {
                printf(RELEASE_DATE "\n");
            }
            else
                printf("ERROR: Unexpected argument!\n");
        }
        else
            printf("ERROR: Expecting a start and end date!\n");
        status = STATUS_HELP;
    }
    else
    {
        Date dates[2];

        bool is_verbose = false;
        int error_pos = 0;
        if (GetDates(argc, argv, dates, is_verbose, error_pos))
        {
            if (is_verbose)
                PrintDaysMonthsAndYears(dates[0], dates[1]);
            else
                PrintDays(dates[0], dates[1]);
            status = STATUS_OK;
        }
        else
        {
            printf("%s date format.\n",
                   error_pos == 0 ? "Invalid start"
                                  : (error_pos == 1 ? "Invalid end" : "Unexpected"));
            status = STATUS_SYNTAX_ERROR;
        }
    }

    return status;
}
