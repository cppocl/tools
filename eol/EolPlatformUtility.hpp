/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TOOLS_EOL_EOLPLATFORMUTILITY_HPP
#define OCL_GUARD_TOOLS_EOL_EOLPLATFORMUTILITY_HPP

#include "../../buffer/EolUtility.hpp"
#include "../../buffer/EolPlatform.hpp"
#include "../../platform/platform.hpp"
#include <cstdlib>

// Ensure numeric_limits<>::min and max don't clash with C defines.
#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

// Note: Include <limits> last to be sure min and max macro are not included later.
#include <limits>

namespace ocl
{

/// Provide ability to interrogate or convert a single file with static functions.
class EolPlatformUtility
{

// Static functions.
public:
    /// Identify the platform for converting files.
    static EolPlatform GetNativePlatform()
    {
        switch (platform_type)
        {
        case PlatformTypes::Windows:
            return EolPlatform::Windows;

        case PlatformTypes::Unix:
        case PlatformTypes::Linux:
        case PlatformTypes::BSD:
        case PlatformTypes::Mac:
        case PlatformTypes::Android:
            return EolPlatform::Unix;

        case PlatformTypes::Unknown:
        default:
            return EolPlatform::Unknown;
        }
    }
};

} // namespace ocl

#endif // OCL_GUARD_TOOLS_EOL_EOLPLATFORMUTILITY_HPP
