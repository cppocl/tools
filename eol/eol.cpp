/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../PrintVersion.hpp"
#include "../Error.hpp"
#include "EolFileParser.hpp"
#include "EolParser.hpp"
#include <cstdint>
#include <cstddef>

#define RELEASE_DATE "2019/01/06 18:00"


int main(int argc, char** argv)
{
    typedef char char_type;
    typedef std::uint64_t size_type;
    typedef ocl::Error<char_type, size_type, 256> error_type;

    error_type error;
    ocl::EolParser<char_type, size_type> parser(error, argc, argv);
    parser.ParseArguments();

    if (!parser.HasError())
    {
        // Parsing of arguments so far successful, but there might be other error conditions.
        // Complete parsing and output results.

        bool is_verbose = parser.IsVerbose();

        if (parser.IsHelp())
            parser.PrintHelp();
        else if (parser.IsVersion())
            ocl::PrintVersion(RELEASE_DATE, nullptr, nullptr, is_verbose);
        else if (parser.GetFilename() != nullptr)
        {
            ocl::EolFileParser<char_type, size_type> file_parser(error, parser);
            file_parser.ParseFiles();
        }
    }

    if (error.HasError())
        parser.LogError("ERROR: %s\n", error.GetErrorMessage());
    return error.GetErrorCode();
}
