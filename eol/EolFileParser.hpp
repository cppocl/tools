/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TOOLS_EOL_EOLFILEPARSER_HPP
#define OCL_GUARD_TOOLS_EOL_EOLFILEPARSER_HPP

#include "../../portable/file/BinaryFile.hpp"
#include "../../buffer/EolUtility.hpp"
#include "../BinaryFileUtility.hpp"
#include "../Error.hpp"
#include "EolParser.hpp"
#include "EolPlatformUtility.hpp"

#include <cstddef>
#include <cstdint>

// Ensure numeric_limits<>::min and max don't clash with C defines.
#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

// Note: Include <limits> last to be sure min and max macro are not included later.
#include <limits>

namespace ocl
{

template<typename CharType = char, typename SizeType = std::size_t, std::size_t const error_size = 256>
class EolFileParser
{
public:
    typedef CharType char_type;
    typedef std::uint64_t size_type;
    typedef EolUtility<char_type, size_type> eol_utility;
    typedef EolParser<char_type, size_type, error_size> eol_parser;
    typedef BinaryFileUtility<char_type, size_type, error_size> binary_file_utility_type;
    typedef typename binary_file_utility_type::binary_file_type binary_file_type;
    typedef Error<char_type, size_type, error_size> error_type;

public:
    EolFileParser(error_type& error, eol_parser const& parser)
        : m_error(error)
        , m_parser(parser)
        , m_source_buffer(nullptr)
        , m_source_file_size(0)
    {
    }

    ~EolFileParser()
    {
        Free(m_source_buffer);
    }

public:
    static bool Allocate(char_type*& buffer, SizeType size)
    {
        return binary_file_utility_type::Allocate(buffer, size);
    }

    static void Free(char_type*& buffer)
    {
        binary_file_utility_type::Free(buffer);
    }

    void ParseFile(char_type const* filename)
    {
        if (!m_error.HasError())
        {
            m_source_file_size = 0;
            bool is_verbose = m_parser.IsVerbose();

            if (filename != nullptr && binary_file_type::Exists(filename))
            {
                binary_file_utility_type::ReadFile(m_error,
                                                   filename,
                                                   m_file,
                                                   m_source_buffer,
                                                   m_source_file_size,
                                                   true);

                if (!m_error.HasError())
                {
                    // Always display filename and size for --verbose.
                    if (is_verbose)
                        m_parser.Log("%s (%lu bytes)\n",
                                     filename,
                                     static_cast<unsigned long>(m_source_file_size));

                    bool convert_file = !LogPlatform();

                    if (LogLinesAndChars() && convert_file)
                        convert_file = false;

                    // Only convert the file when the platform,
                    // lines and char count has not been logged.
                    if (convert_file)
                        ConvertFile(filename);

                    if (!m_file.Close() && !m_parser.HasError())
                        m_error.SetErrorCode(ErrorCode::FileClose, filename);
                }
            }
            else
                m_error.SetErrorCode(ErrorCode::FileMissing, filename);
        }
    }

    void ParseFiles()
    {
        char_type const* filename = m_parser.GetFilename();
        ParseFile(filename);
    }

private:
    /// Display detected platform for --platform or --verbose.
    bool LogPlatform()
    {
        bool log_platform = m_parser.IsVerbose() || m_parser.IsPlatform();

        if (log_platform)
        {
            EolPlatform platform = eol_utility::GetPlatform(m_source_buffer, m_source_file_size);
            m_parser.Log("%s\n", eol_utility::GetPlatformAsString(platform));
        }

        return log_platform;
    }

    /// Display lines and end of line characters for --lines, --chars or --verbose.
    bool LogLinesAndChars()
    {
        typedef ocl::EolUtility<char, size_type> eol_utility;
        size_type lines = 0;
        size_type chars = 0;
        if (m_parser.IsVerbose() || m_parser.IsChars())
            lines = eol_utility::CountLines(m_source_buffer, m_source_file_size, chars);
        else if (m_parser.IsLines())
            lines = eol_utility::CountLines(m_source_buffer, m_source_file_size);

        bool logged = false;
        if (lines > 0)
        {
            m_parser.Log("lines: %lu\n", static_cast<unsigned long>(lines));
            logged = true;
        }
        if (chars > 0)
        {
            m_parser.Log("end of line characters: %lu\n", static_cast<unsigned long>(chars));
            logged = true;
        }

        return logged;
    }

    /// Get target platform from --unix, --other, --windows or --other.
    /// Return EolPlatform::Unknown if not specified or native platform not known.
    EolPlatform GetTargetPlatform()
    {
        EolPlatform target_platform = EolPlatform::Unknown;

        if (m_parser.ToUnix())
            target_platform = EolPlatform::Unix;
        else if (m_parser.ToOther())
            target_platform = EolPlatform::Other;
        else if (m_parser.ToWindows())
            target_platform = EolPlatform::Windows;
        else if (m_parser.ToNative())
        {
            target_platform = EolPlatformUtility::GetNativePlatform();
            if (target_platform == EolPlatform::Unknown)
                m_error.SetError(ErrorCode::Unexpected, "Native platform is not known");
        }
        else
            m_error.SetError(ErrorCode::MissingArgument,
                             "Expecting --platform, --unix, --other, --windows or --native");

        return target_platform;
    }

    /// Convert the file to the target platform.
    void ConvertFile(char_type const* filename)
    {
        EolPlatform target_platform = GetTargetPlatform();

        // Only write the file when the platform is different.
        if (filename != nullptr && !m_parser.HasError())
        {
            EolPlatform source_platform = eol_utility::GetPlatform(m_source_buffer,
                                                                   m_source_file_size);

            if (source_platform != target_platform)
            {
                size_type new_file_size = eol_utility::GetBufferCount(m_source_buffer,
                                                                      m_source_file_size,
                                                                      target_platform);

                if (new_file_size > 0)
                {
                    char* new_buffer = nullptr;
                    if (Allocate(new_buffer, new_file_size))
                    {
                        eol_utility::ConvertEol(new_buffer,
                                                new_file_size,
                                                m_source_buffer,
                                                m_source_file_size,
                                                target_platform);

                        if ((new_file_size < m_source_file_size) &&
                            !m_file.SetFileSize(new_file_size))
                        {
                            m_error.SetErrorCode(ErrorCode::FileChangeSize, filename);
                        }

                        if (!m_parser.HasError())
                            if (!m_file.Seek(0) || !m_file.Write(new_buffer, new_file_size))
                                m_error.SetError(ErrorCode::Unexpected,
                                                 "Failed to write to file",
                                                 filename);

                        Free(new_buffer);
                    }
                    else
                        m_error.SetError(ErrorCode::Unexpected,
                                         "Cannot allocate memory for file: ",
                                         filename);
                }
                else
                    m_error.SetError(ErrorCode::Unexpected,
                                     "Unable to calculate m_source_buffer size for file:",
                                     filename);
            }
        }
    }

// Data
private:
    error_type& m_error;
    eol_parser const& m_parser;
    binary_file_type m_file;
    char* m_source_buffer;
    size_type m_source_file_size;
};

} // namespace ocl

#endif // OCL_GUARD_TOOLS_EOL_EOLFILEPARSER_HPP
