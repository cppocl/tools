/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TOOLS_EOL_EOLPARSER_HPP
#define OCL_GUARD_TOOLS_EOL_EOLPARSER_HPP

#include "../../portable/file/BinaryFile.hpp"
#include "../OptionUtility.hpp"
#include "../Error.hpp"
#include "EolParseError.hpp"
#include <cstdio>
#include <cstring>
#include <stdarg.h>

namespace ocl
{

template<typename CharType = char, typename SizeType = std::size_t, std::size_t const size = 256>
class EolParser
{
public:
    typedef CharType char_type;
    typedef SizeType size_type;
    typedef Error<char_type, size_type, 256> error_type;

public:
    EolParser(error_type& error, int argc, char_type** argv)
        : m_error(error)
        , m_argc(argc)
        , m_argv(argv)
        , m_filename(nullptr)
        , m_log_filename(nullptr)
        , m_help(false)
        , m_recursive(false)
        , m_quiet(false)
        , m_verbose(false)
        , m_version(false)
        , m_to_windows(false)
        , m_to_unix(false)
        , m_to_other(false)
        , m_to_native(false)
        , m_platform(false)
        , m_lines(false)
        , m_chars(false)
        , m_native(false)
        , m_non_native(false)
    {
    }

    ~EolParser()
    {
    }

    EolParser(EolParser const&) = delete;
    EolParser& operator =(EolParser const&) = delete;
    EolParser(EolParser&&) = delete;
    EolParser& operator =(EolParser&&) = delete;

public:

    /// Parse the arguments provided within the constructor, and return the status.
    /// NOTE: Parse will not perform any recursion, only parse the arguments.
    void ParseArguments()
    {
        bool duplicate = false;
        char_type const* log_filename = nullptr;

        for (int pos = 1; pos < m_argc && !HasError(); ++pos)
        {
            char_type const* arg = m_argv[pos];

            if (*arg != '-')
            {
                if (m_filename == nullptr)
                    m_filename = arg;
                else
                    m_error.SetError(ErrorCode::TooManyArguments, "Too many filenames", arg);
            }
            else
            {
                if (OptionsUtility::GetLogFile(arg, log_filename, duplicate) ||
                    OptionsUtility::IsHelp(arg, m_help, duplicate) ||
                    OptionsUtility::IsVersion(arg, m_version, duplicate) ||
                    OptionsUtility::IsQuiet(arg, m_quiet, duplicate) ||
                    OptionsUtility::IsVerbose(arg, m_verbose, duplicate) ||
                    OptionsUtility::IsSet(arg, "--unix", m_to_unix, duplicate) ||
                    OptionsUtility::IsSet(arg, "--other", m_to_other, duplicate) ||
                    OptionsUtility::IsSet(arg, "--windows", m_to_windows, duplicate) ||
                    OptionsUtility::IsSet(arg, "--native", m_to_native, duplicate) ||
                    OptionsUtility::IsSet(arg, "--platform", m_platform, duplicate) ||
                    OptionsUtility::IsSet(arg, "--lines", m_lines, duplicate) ||
                    OptionsUtility::IsSet(arg, "--chars", m_chars, duplicate) ||
                    OptionsUtility::IsSet(arg, "--list-native", m_native, duplicate) ||
                    OptionsUtility::IsSet(arg, "--list-non-native", m_non_native, duplicate))
                {
                    SetErrorOnDuplicate(arg, duplicate);
                }
                else
                {
                    // if the argument starts with '-' and is not a recognised option,
                    // ensure it's not a file before recording the error.
                    if (m_filename == nullptr)
                    {
                        m_filename = internalSelectFile();
                        if (m_filename == nullptr)
                            m_error.SetErrorCode(ErrorCode::UnexpectedArgument, arg);
                    }
                    else
                        m_error.SetErrorCode(ErrorCode::UnexpectedArgument, arg);
                }
            }
        }

        if (!HasError())
            ValidateArguments();
    }

    /// Additional parsing to verify the combination of arguments.
    void ValidateArguments()
    {
        if (m_help || m_version)
        {
            // Handle options that don't require a filename.
            if (m_filename != nullptr)
                m_error.SetError(ErrorCode::IncompatibleArgument,
                                 "Filename cannot be specified with --help or --version");
            else if (m_help && m_verbose)
                m_error.SetError(ErrorCode::IncompatibleArgument,
                                 "There is no meaning for --help and --verbose together");
        }
        else if (m_filename == nullptr)
        {
            m_filename = internalSelectFile();
            if (m_filename == nullptr)
                m_error.SetErrorCode(ErrorCode::NoFile);
        }

        if (!HasError() && m_filename != nullptr)
        {
            int to_count = 0;
            if (ToUnix())
                ++to_count;
            if (ToOther())
                ++to_count;
            if (ToWindows())
                ++to_count;
            if (to_count > 1)
                m_error.SetError(ErrorCode::IncompatibleArgument,
                                 "Cannot use --unix, --other and --windows together");
        }
    }

// Status functions.
public:
    /// When the filename is passed as an argument, return the non-null filename.
    /// If the filename is null, then only options valid without a filename are used.
    char_type const* GetFilename() const
    {
        return m_filename;
    }

    /// Get the log filename, which is extracted from --log=
    char_type const* GetLogFilename() const
    {
        return m_log_filename;
    }

    /// Change the filename, to handle recursive file handling.
    void SetFilename(char_type const* filename)
    {
        m_filename = filename;
    }

    /// Only set the error code and message when the argument is duplicated.
    void SetErrorOnDuplicate(bool duplicate)
    {
        if (duplicate)
            m_error.SetErrorCode(ErrorCode::DuplicateArgument);
    }

    /// Only set the error code, message with argument when the argument is duplicated.
    void SetErrorOnDuplicate(char_type const* argument, bool duplicate)
    {
        if (duplicate)
            m_error.SetErrorCode(ErrorCode::DuplicateArgument, argument);
    }

    bool HasError() const noexcept
    {
        return m_error.HasError();
    }

    /// Get the error after parsing.
    error_type const & GetError() const noexcept
    {
        return m_error;
    }

    /// Get the error after parsing.
    error_type& GetError() noexcept
    {
        return m_error;
    }

    /// Get the error code after parsing.
    int GetErrorCode() const
    {
        return m_error.GetErrorCode();
    }

    /// Return the error message, which will be "" when there is no error.
    char_type const* GetErrorMessage() const
    {
        return m_error.GetErrorMessage();
    }

    /// Return true when option --help is specified.
    bool IsHelp() const
    {
        return m_help;
    }

    /// Return true when option --recursive is specified.
    bool IsRecursive() const
    {
        return m_recursive;
    }

    /// Return true when option --quiet is specified.
    bool IsQuiet() const
    {
        return m_quiet;
    }

    /// Return true when option --verbose is specified.
    bool IsVerbose() const
    {
        return m_verbose;
    }

    /// Return true when option --version is specified.
    bool IsVersion() const
    {
        return m_version;
    }

    /// Return true when option --windows is specified.
    bool ToWindows() const
    {
        return m_to_windows;
    }

    /// Return true when option --unix is specified.
    bool ToUnix() const
    {
        return m_to_unix;
    }

    /// Return true when option --other is specified.
    bool ToOther() const
    {
        return m_to_other;
    }

    /// Return true when option --other is specified.
    bool ToNative() const
    {
        return m_to_native;
    }

    //// Return true when option --platform is specified.
    bool IsPlatform() const
    {
        return m_platform;
    }

    //// Return true when option --lines is specified (for number of lines).
    bool IsLines() const
    {
        return m_lines;
    }

    //// Return true when option --chars is specified (for number of end of line characters).
    bool IsChars() const
    {
        return m_chars;
    }

    void Log(const char_type* fmt, ...) const
    {
        va_list vl;
        va_start(vl, fmt);
        if (!IsQuiet() && GetLogFilename() == nullptr)
            ::vprintf(fmt, vl);
        va_end(vl);
    }

    void LogError(const char_type* fmt, ...) const
    {
        va_list vl;
        va_start(vl, fmt);
        if (!IsQuiet() && GetLogFilename() == nullptr)
            ::vfprintf(stderr, fmt, vl);
        va_end(vl);
    }

// Output functions.
public:
    void PrintHelp()
    {
        Log("Syntax:\n"
            "    eol <filename> "

            // options that work without a filename.
                               "[-h | --help]\n"
            "                   [-v | --version]\n"

            // options that work with or without a filename.
            "                   [-l | --log] (not implemented)\n"
            "                   [--verbose]\n"

            // options that work with a filename.
            "                   [-r | --recursive] (not implemented)\n"
            "                   [--list-native] (not implemented)\n"
            "                   [--list-non-native] (not implemented)\n"
            "                   [--windows]\n"
            "                   [--unix]\n"
            "                   [--other]\n"
            "                   [--native]\n"
            "                   [--lines]\n"
            "                   [--chars]\n\n"
            "Information:\n"
            "    Display end of line information about a file, or convert end of line characters.\n"
            "    The default behavior when there are no specified options is to output the detected platform.\n"
            "    Platform options can be Windows (\\r\\n), Unix (\\n), Other (\\r), Mixed or Unknown.\n"
            "    NOTE: Unknown is output when there are no line endings within a file.\n\n"
            "    --verbose          Display platform, number of lines and number of end of line characters.\n"
            "    --recursive        Apply query or conversion recursively to matching files.\n"
            "    --list-native      Filter natives files for output.\n"
            "    --list-non-native  Filter non-natives files for output.\n"
            "    --windows          Convert file to Windows (\\r\\n) line endings.\n"
            "    --unix             Convert file to Unix (\\n) line endings.\n"
            "    --other            Convert file to other (\\r) line endings.\n"
            "    --native           Convert file to native platform line endings.\n"
            "    --platform         Output the platform for the filename, e.g. Unix or Windows\n"
            "    --lines            Output number of lines.\n"
            "    --chars            Output number of end of line characters.\n\n"
            "e.g.\n"
            "    eol readme.txt\n"
            "    eol readme.txt --verbose\n"
            "    eol readme.txt --windows\n");
    }

private:
    /// This is only used when a filename begins with '-', and so needs to be detected.
    char_type const* internalSelectFile()
    {
        for (int pos = 1; pos < m_argc && !HasError(); ++pos)
        {
            char_type const* arg = m_argv[pos];

            if (ocl::BinaryFile<char_type>::Exists(arg))
            {
                m_filename = arg;
                break;
            }
        }

        return nullptr;
    }

private:
    error_type& m_error;             /// Error code and message.
    int m_argc;                      /// argc from function main.
    char_type** m_argv;              /// argv from function main.
    char_type const* m_filename;     /// Specified filename.
    char_type const* m_log_filename; /// Specified log filename.
    bool m_help;                     /// Output the help  when true.
    bool m_recursive;                /// Recursively apply end of line optioss to files in sub-folders when true.
    bool m_quiet;                    /// Hide all output when true.
    bool m_verbose;                  /// Either provide verbose information for version or file when true.
    bool m_version;                  /// Display version information for eol utility when true.
    bool m_to_windows;               /// Convert line endings to Windows (\r\n) when true.
    bool m_to_unix;                  /// Convert line endings to Unix (\n) when true.
    bool m_to_other;                 /// Convert line endings to other (\r) when true.
    bool m_to_native;                /// Convert line endings to native platform.
    bool m_platform;                 /// Output the platform for the filename, e.g. Unix or Windows.
    bool m_lines;                    /// Count number of lines when true.
    bool m_chars;                    /// Count end of line characters when true.
    bool m_native;                   /// Filter output for native files when true.
    bool m_non_native;               /// Filter output for non-native files when true.
};

} // namespace ocl

#endif // OCL_GUARD_TOOLS_EOL_EOLPARSER_HPP
