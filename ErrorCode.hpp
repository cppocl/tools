/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TOOLS_ERRORCODE_HPP
#define OCL_GUARD_TOOLS_ERRORCODE_HPP

#include <cerrno>

namespace ocl
{

/// Define error codes which can be returned by function main.
enum class ErrorCode : int
{
    None = 0,

    /// Start for any error codes that should be defined by <cerrno>, but are missing.
    CustomErrnoStart = 1000,

    /// Start for any error codes that not defined by <cerrno>.
    CustomErrorStart = 2000,


    ///
    /// Generic errors.
    ///
    Busy =
#ifdef EBUSY
    EBUSY,
#else
    CustomErrnoStart + 1,
#endif

    InProgress =
#ifdef EALREADY
    EALREADY,
#else
    CustomErrnoStart + 2,
#endif

    Retry =
#ifdef EAGAIN
    EAGAIN,
#else
    CustomErrnoStart + 3,
#endif

    Cancelled =
#ifdef ECANCELED
    ECANCELED,
#else
    CustomErrnoStart + 4,
#endif

    PermissionDenied =
#ifdef EACCES
    EACCES,
#else
    CustomErrnoStart + 5,
#endif

    OutOfRange =
#ifdef ERANGE
    ERANGE,
#else
    CustomErrnoStart + 6,
#endif

    IdentifierRemoved =
#ifdef EIDRM
    EIDRM,
#else
    CustomErrnoStart + 7,
#endif

    IllegalByteSequence =
#ifdef EILSEQ
    EILSEQ,
#else
    CustomErrnoStart + 8,
#endif

    OperationInProgress =
#ifdef EINPROGRESS
    EINPROGRESS,
#else
    CustomErrnoStart + 9,
#endif

    InterruptedFunction =
#ifdef EINTR
    EINTR,
#else
    CustomErrnoStart + 10,
#endif

    Unrecoverable =
#ifdef ENOTRECOVERABLE
    ENOTRECOVERABLE,
#else
    CustomErrnoStart + 11,
#endif


    ///
    /// Errors for argument parsing.
    ///
    NumberOfArguments =
#ifdef E2BIG
    E2BIG,
#else
    CustomErrnoStart + 12,
#endif

    InvalidArgument =
#ifdef EINVAL
    EINVAL,
#else
    CustomErrnoStart + 13,
#endif


    ///
    /// File or hardware errors.
    ///
    IOError =
#ifdef EIO
    EIO,
#else
    CustomErrnoStart + 14,
#endif

    IsDirectory =
#ifdef EISDIR
    EISDIR,
#else
    CustomErrnoStart + 15,
#endif

    NotDirectory =
#ifdef ENOTDIR
    ENOTDIR,
#else
    CustomErrnoStart + 16,
#endif

    DirectoryNotEmpty =
#ifdef ENOTEMPTY
    ENOTEMPTY,
#else
    CustomErrnoStart + 17,
#endif

    TooManyOpenFiles =
#ifdef ENFILE
    ENFILE,
#else
    CustomErrnoStart + 18,
#endif

    FileBad =
#ifdef EBADF
    EBADF,
#else
    CustomErrnoStart + 19,
#endif

    FileTooBig =
#ifdef EFBIG
    EFBIG,
#else
    CustomErrnoStart + 20,
#endif

    FilenameTooLong =
#ifdef EMFILE
    EMFILE,
#else
    CustomErrnoStart + 21,
#endif

    FileExists =
#ifdef EEXIST
    EEXIST,
#else
    CustomErrnoStart + 22,
#endif

    FileMissing =
#ifdef ENOENT
    ENOENT,
#else
    CustomErrnoStart + 23,
#endif

    FileBusy =
#ifdef ETXTBSY
    ETXTBSY,
#else
    CustomErrnoStart + 24,
#endif

    DeviceFull =
#ifdef ENOSPC
    ENOSPC,
#else
    CustomErrnoStart + 25,
#endif

    ReadOnlyFileSysren =
#ifdef EROFS
    EROFS,
#else
    CustomErrnoStart + 26,
#endif

    InvalidSeek =
#ifdef ESPIPE
    ESPIPE,
#else
    CustomErrnoStart + 27,
#endif

    DeviceMissing =
#ifdef ENODEV
    ENODEV,
#else
    CustomErrnoStart + 28,
#endif


    ///
    /// Memory errors.
    ///
    InvalidAddress =
#ifdef EFAULT
    EFAULT,
#else
    CustomErrnoStart + 29,
#endif

    AddressUsed =
#ifdef EADDRINUSE
    EADDRINUSE,
#else
    CustomErrnoStart + 30,
#endif

    AddressUnavailable =
#ifdef EADDRNOTAVAIL
    EADDRNOTAVAIL,
#else
    CustomErrnoStart + 31,
#endif

    OutOfMemory =
#ifdef ENOMEM
    ENOMEM,
#else
    CustomErrnoStart + 32,
#endif

    DestinationAddressRequired =
#ifdef EDESTADDRREQ
    EDESTADDRREQ,
#else
    CustomErrnoStart + 33,
#endif

    BufferFull =
#ifdef ENOBUFS
    ENOBUFS,
#else
    CustomErrnoStart + 34,
#endif


    ///
    /// Process errors.
    ///
    ProcessNotFound =
#ifdef ESRCH
    ESRCH,
#else
    CustomErrnoStart + 35,
#endif

    ChildProcessNotFound =
#ifdef ECHILD
    ECHILD,
#else
    CustomErrnoStart + 36,
#endif

    DeadlockWarning =
#ifdef EDEADLK
    EDEADLK,
#else
    CustomErrnoStart + 37,
#endif


    ///
    /// Messaging errors.
    ///
    MessageBad =
#ifdef EBADMSG
    EBADMSG,
#else
    CustomErrnoStart + 38,
#endif

    MessageInvalidSize =
#ifdef EMSGSIZE
    EMSGSIZE,
#else
    CustomErrnoStart + 39,
#endif

    MessageUnavailable =
#ifdef ENODATA
    ENODATA,
#else
    CustomErrnoStart + 40,
#endif

    MessageInvalid =
#ifdef ENOMSG
    ENOMSG,
#else
    CustomErrnoStart + 41,
#endif

    ProtocolUnavailable =
#ifdef ENOPROTOOPT
    ENOPROTOOPT,
#else
    CustomErrnoStart + 42,
#endif


    ///
    /// Connection and network errors.
    ///
    ConnectionAborted =
#ifdef ECONNABORTED
    ECONNABORTED,
#else
    CustomErrnoStart + 43,
#endif

    ConnectionRefused =
#ifdef ECONNREFUSED
    ECONNREFUSED,
#else
    CustomErrnoStart + 44,
#endif

    ConnectionReset =
#ifdef ECONNRESET
    ECONNRESET,
#else
    CustomErrnoStart + 45,
#endif

    HostUnreachable =
#ifdef EHOSTUNREACH
    EHOSTUNREACH,
#else
    CustomErrnoStart + 46,
#endif

    SocketConnected =
#ifdef EISCONN
    EISCONN,
#else
    CustomErrnoStart + 47,
#endif

    SocketNotConnected =
#ifdef ENOTCONN
    ENOTCONN,
#else
    CustomErrnoStart + 48,
#endif


    ///
    /// Custom generic errors.
    ///
    NotImplemented = CustomErrorStart + 1, // 2001
    Unexpected,                            // 2002
    Syntax,                                // 2003
    Bad,                                   // 2004
    Aborted,                               // 2005
    Exists,                                // 2006
    Missing,                               // 2007
    TooSmall,                              // 2008
    TooLarge,                              // 2009

    ///
    /// Custom errors for argument parsing.
    ///
    NotEnoughArguments,                    // 2010
    TooManyArguments,                      // 2011
    DuplicateArgument,                     // 2012
    UnexpectedArgument,                    // 2013
    MissingArgument,                       // 2014
    IncompatibleArgument,                  // 2015

    ///
    /// Custom file or hardware errors.
    ///
    IsFile,                                // 2016
    NoFile,                                // 2017
    FileEmpty,                             // 2018
    FileTooSmall,                          // 2019
    FilenameInvalid,                       // 2020
    FileNotFound,                          // 2021
    FileRead,                              // 2022
    FileWrite,                             // 2023
    FileSeek,                              // 2024
    FileOpen,                              // 2025
    FileClose,                             // 2026
    FileChangeSize,                        // 2027
    FileReadOutOfMemory,                   // 2028
    DirectoryExists,                       // 2029
    DirectoryMissing,                      // 2030
    DirectoryNotFound,                     // 2031

    ///
    /// Custom memory errors.
    ///
    SourceAddressRequired,                 // 2032
};

} // namespace ocl

#endif // OCL_GUARD_TOOLS_ERRORCODE_HPP
