Always available on every tool:
-h or --help               Help for command line tool.
-v or --version            Version information, default is full version information.
-t or --time               Elapsed running time for tool, default hh:mm:ss.             (format: -t=ss, mm:ss, hh:mm:ss)
-f or --file               Place output into file.                                      (format: -f=filename.log)
-q or --quiet              No information output to terminal or screen.

Optional for tools:
-p or --processes          Number of processes (or cores) to use, default all.          (format: -p=<count>  e.g. -p=4)
-r or --recursive          Recursive searching for file tools, default depth infinity.  (format: -r=<depth>  e.g. -r=2)
-s or --sort               Sort ascending or descending, default ascending.             (format: -s=a, d, ascending or descending)
-u or --unique             Duplicate matching results are removed.

--type                     Filter all files matching extensions.                        (format: --type=ext;ext;ext...)
--verbose                  Full information for output.
