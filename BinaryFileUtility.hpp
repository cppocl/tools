/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TOOLS_BINARYFILEUTILITY_HPP
#define OCL_GUARD_TOOLS_BINARYFILEUTILITY_HPP

#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

#include "Error.hpp"
#include "../portable/file/BinaryFile.hpp"

namespace ocl
{

/// Common binary file utility functions.
template<typename CharType = char, typename SizeType = std::size_t, std::size_t const error_size = 256>
class BinaryFileUtility
{
public:
    typedef CharType char_type;
    typedef SizeType size_type;
    typedef Error<char_type, size_type, error_size> error_type;
    typedef BinaryFile<char_type> binary_file_type;

    static constexpr std::size_t ERROR_SIZE = error_size;

public:
    template<typename Type>
    static bool Allocate(Type*& buffer, SizeType size)
    {
        ::free(buffer);
        buffer = static_cast<char*>(::malloc(static_cast<size_t>(size) * sizeof(Type)));
        return buffer != nullptr;
    }

    template<typename Type>
    static void Free(Type*& buffer)
    {
        ::free(buffer);
        buffer = nullptr;
    }

    /// Read the contents of a file into the buffer.
    static void ReadFile(error_type& error,
                         char_type const* filename,
                         binary_file_type& bin_file,
                         char*& buffer,
                         size_type& file_size,
                         bool keep_open = false)
    {
        if (filename != nullptr)
        {
            if (binary_file_type::Exists(filename))
            {
                bool seek_to_start = bin_file.IsOpen();
                if (seek_to_start || bin_file.Open(filename))
                {
                    if (seek_to_start)
                        bin_file.SeekToStart();
                    file_size = bin_file.GetFileSize();
                    if (file_size > 0)
                    {
                        if (file_size <= static_cast<size_type>(std::numeric_limits<size_t>::max()))
                        {
                            if (Allocate(buffer, file_size))
                            {
                                if (!bin_file.Read(buffer, file_size))
                                    error.SetErrorCode(ErrorCode::FileRead, filename);
                                else if (!keep_open && !bin_file.Close())
                                    error.SetErrorCode(ErrorCode::FileClose, filename);
                            }
                            else
                                error.SetErrorCode(ErrorCode::FileReadOutOfMemory, filename);
                        }
                        if (!keep_open && !bin_file.Close())
                            error.SetErrorCode(ErrorCode::FileClose, filename);
                    }
                    else
                        error.SetErrorCode(ErrorCode::FileEmpty, filename);
                }
                else
                    error.SetErrorCode(ErrorCode::FileOpen, filename);
            }
            else
                error.SetErrorCode(ErrorCode::FileMissing, filename);
        }
        else
            error.SetErrorCode(ErrorCode::FilenameInvalid, "");
    }
};

} // namespace ocl

#endif // OCL_GUARD_TOOLS_BINARYFILEUTILITY_HPP
