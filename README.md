# tools

![](header_image.jpg)

## Overview

Command line tools demonstrating the use of the projects within cppocl.

## Samples

### datediff

```bash
datediff 1980-01-14 now
```
