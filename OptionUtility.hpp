/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TOOLS_OPTIONUTILITY_HPP
#define OCL_GUARD_TOOLS_OPTIONUTILITY_HPP

#include <cstring>

namespace ocl
{

class OptionsUtility
{
public:
    static bool IsSet(char const* arg,
                      char const* option,
                      bool& current_option_state,
                      bool& duplicate)
    {
        bool is_equal = ::strcmp(arg, option) == 0;
        return SetDuplicateWhenSame(is_equal, current_option_state, duplicate);
    }

    static bool IsSet(char const* arg,
                      char const* option,
                      char const* long_option,
                      bool& current_option_state,
                      bool& duplicate)
    {
        bool is_equal = ::strcmp(arg, option) == 0 || ::strcmp(arg, long_option) == 0;
        return SetDuplicateWhenSame(is_equal, current_option_state, duplicate);
    }

    static bool IsHelp(char const* arg, bool& current_option_state, bool& duplicate)
    {
        return IsSet(arg, "-h", "--help", current_option_state, duplicate);
    }

    static bool IsVersion(char const* arg, bool& current_option_state, bool& duplicate)
    {
        return IsSet(arg, "-v", "--version", current_option_state, duplicate);
    }

    static bool IsRecursive(char const* arg, bool& current_option_state, bool& duplicate)
    {
        return IsSet(arg, "-l", "--log", current_option_state, duplicate);
    }

    static bool GetLogFile(char const* arg, char const*& curr_log_filename, bool& duplicate)
    {
        std::size_t len = 3;
        bool is_set = ::memcmp(arg, "-l=", len) == 0;
        if (!is_set)
        {
            len = 5;
            is_set = ::memcmp(arg, "-log=", 5) == 0;
        }
        if (is_set)
        {
            if (curr_log_filename != nullptr)
                duplicate = true;
            else
            {
                arg += len;
                curr_log_filename = arg;
            }
        }
        return is_set;
    }

    static bool IsTime(char const* arg, bool& current_option_state, bool& duplicate)
    {
        return IsSet(arg, "-t", "--time", current_option_state, duplicate);
    }

    static bool IsSortAscending(char const* arg, bool& current_option_state, bool& duplicate)
    {
        bool optiom_equal = ::strcmp(arg, "-s") == 0 ||
                            ::strcmp(arg, "-s=a") == 0 ||
                            ::strcmp(arg, "-s=ascending") == 0 ||
                            ::strcmp(arg, "--sort") == 0 ||
                            ::strcmp(arg, "--sort=a") == 0 ||
                            ::strcmp(arg, "--sort=ascending") == 0;
        return SetDuplicateWhenSame(optiom_equal, current_option_state, duplicate);
    }

    static bool IsSortDescending(char const* arg, bool& current_option_state, bool& duplicate)
    {
        bool optiom_equal = ::strcmp(arg, "-s=d") == 0 ||
                            ::strcmp(arg, "-s=descending") == 0 ||
                            ::strcmp(arg, "--sort=d") == 0 ||
                            ::strcmp(arg, "--sort=descending") == 0;
        return SetDuplicateWhenSame(optiom_equal, current_option_state, duplicate);
    }

    static bool IsType(char const* arg, bool& current_option_state, bool& duplicate)
    {
        return IsSet(arg, "--type", current_option_state, duplicate);
    }

    static bool IsQuiet(char const* arg, bool& current_option_state, bool& duplicate)
    {
        return IsSet(arg, "--quiet", current_option_state, duplicate);
    }

    static bool IsVerbose(char const* arg, bool& current_option_state, bool& duplicate)
    {
        return IsSet(arg, "--verbose", current_option_state, duplicate);
    }

private:
    /// Copy optiom_equal into current_option_state, and set duplicate as true,
    /// if optiom_equal and current_option_state are both true.
    static bool SetDuplicateWhenSame(bool optiom_equal,
                                     bool& current_option_state,
                                     bool& duplicate)
    {
        // Set duplicate true when arg is found two or more times.
        if (optiom_equal)
        {
            if (current_option_state)
                duplicate = true;
            else
                // Only set the boolean from false to true, to retain tracking of set arguments.
                current_option_state = optiom_equal;
        }

        return optiom_equal;
    }
};

} // namespace ocl

#endif // OCL_GUARD_TOOLS_OPTIONUTILITY_HPP
