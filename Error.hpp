/*
Copyright 2019 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TOOLS_ERROR_HPP
#define OCL_GUARD_TOOLS_ERROR_HPP

#include "ErrorCode.hpp"
#include "../string/FixedString.hpp"
#include <cstring>
#include <cstddef>

namespace ocl
{

template<typename CharType = char, typename SizeType = std::size_t, std::size_t const size = 256>
class Error
{
public:
    typedef CharType char_type;
    typedef SizeType size_type;
    typedef FixedString<char_type, size_type, size> string_type;

public:
    Error(ErrorCode error_code = ErrorCode::None,
          char_type const* error_message = nullptr,
          char_type const* argument = nullptr,
          char_type const* separator = nullptr)
        : m_error_code(error_code)
        , m_separator(separator != nullptr ? separator : ": ")
    {
        if (error_message != nullptr)
            SetErrorCode(error_code);
        else
            m_error_message.Copy(error_message,
                                 argument != nullptr ? m_separator : nullptr,
                                 argument);
    }

    Error(Error const& error)
        : m_error_code(error.m_error_code)
        , m_separator(error.m_separator)
        , m_error_message(error.m_error_message)
    {
    }

    Error(Error&& error)
        : m_error_code(error.m_error_code)
        , m_separator(error.m_separator)
        , m_error_message(static_cast<string_type&&>(error.m_error_message))
    {
        error.m_error_code = ErrorCode::None;
        error.m_separator = nullptr;
    }

    ~Error()
    {
    }

    Error& operator=(Error const& error)
    {
        m_error_code = error.m_error_code;
        m_separator = error.m_separator;
        m_error_message.Copy(error.m_error_message);
        return *this;
    }

    Error& operator=(Error&& error)
    {
        m_error_code = error.m_error_code;
        error.m_error_code = ErrorCode::None;
        m_separator = error.m_separator;
        error.m_separator = nullptr;
        m_error_message.Move(error.m_error_message);
        return *this;
    }

    bool HasError() const noexcept
    {
        return m_error_code != ErrorCode::None;
    }

    ErrorCode GetError() const noexcept
    {
        return m_error_code;
    }

    int GetErrorCode() const noexcept
    {
        return static_cast<int>(m_error_code);
    }

    char_type const* GetErrorMessage() const noexcept
    {
        return static_cast<char_type const*>(m_error_message);
    }

    void ClearError()
    {
        m_error_code = ErrorCode::None;
        m_error_message.Clear();
    }

    void SetError(ErrorCode error_code, char_type const* error_message)
    {
        m_error_code = error_code;
        m_error_message.Copy(error_message);
    }

    /// Set the error code, message and argument.
    /// If argument is null then the error message will be set without the argument.
    void SetError(ErrorCode error_code,
                  char_type const* error_message,
                  char_type const* argument)
    {
        m_error_code = error_code;
        if (argument != nullptr)
            m_error_message.Copy(error_message, m_separator, argument);
        else
            m_error_message.Copy(error_message);
    }

    /// Set the error code, and the message string using a default message,
    /// selected by the error code, and optionally including a formatted arguments.
    void SetErrorCode(ErrorCode error_code, char_type const* argument = nullptr)
    {
        switch (error_code)
        {
        /// Generic errors.
        case ErrorCode::Busy:
            SetError(error_code, "Busy", argument);
            break;

        case ErrorCode::InProgress:
            SetError(error_code, "In progress", argument);
            break;

        case ErrorCode::Retry:
            SetError(error_code, "Retry", argument);
            break;

        case ErrorCode::Cancelled:
            SetError(error_code, "Cancelled", argument);
            break;

        case ErrorCode::PermissionDenied:
            SetError(error_code, "Permission denied", argument);
            break;

        case ErrorCode::OutOfRange:
            SetError(error_code, "Out of range", argument);
            break;

        case ErrorCode::IdentifierRemoved:
            SetError(error_code, "Identifier removed", argument);
            break;

        case ErrorCode::IllegalByteSequence:
            SetError(error_code, "Illegal byte sequence", argument);
            break;

        case ErrorCode::OperationInProgress:
            SetError(error_code, "Operation in progress", argument);
            break;

        case ErrorCode::InterruptedFunction:
            SetError(error_code, "Interrupted function", argument);
            break;

        case ErrorCode::Unrecoverable:
            SetError(error_code, "Unrecoverable", argument);
            break;

        /// Errors for argument parsing.
        case ErrorCode::NumberOfArguments:
            SetError(error_code, "Invalid number of arguments", argument);
            break;

        case ErrorCode::InvalidArgument:
            SetError(error_code, "Invalid argument", argument);
            break;

        /// File or hardware errors.
        case ErrorCode::IOError:
            SetError(error_code, "I/O error", argument);
            break;

        case ErrorCode::IsDirectory:
            SetError(error_code, "Is directory", argument);
            break;

        case ErrorCode::NotDirectory:
            SetError(error_code, "Not a directory", argument);
            break;

        case ErrorCode::DirectoryNotEmpty:
            SetError(error_code, "Directory not empty", argument);
            break;

        case ErrorCode::TooManyOpenFiles:
            SetError(error_code, "Too many open files", argument);
            break;

        case ErrorCode::FileBad:
            SetError(error_code, "File is bad", argument);
            break;

        case ErrorCode::FileTooBig:
            SetError(error_code, "File too big", argument);
            break;

        case ErrorCode::FilenameTooLong:
            SetError(error_code, "Filename too long", argument);
            break;

        case ErrorCode::FileExists:
            SetError(error_code, "File already exists", argument);
            break;

        case ErrorCode::FileMissing:
            SetError(error_code, "File missing", argument);
            break;

        case ErrorCode::FileBusy:
            SetError(error_code, "File busy", argument);
            break;

        case ErrorCode::DeviceFull:
            SetError(error_code, "Device full", argument);
            break;

        case ErrorCode::ReadOnlyFileSysren:
            SetError(error_code, "Read-only file system", argument);
            break;

        case ErrorCode::InvalidSeek:
            SetError(error_code, "Invalid seek", argument);
            break;

        case ErrorCode::DeviceMissing:
            SetError(error_code, "Device missing", argument);
            break;

        /// Memory errors.
        case ErrorCode::InvalidAddress:
            SetError(error_code, "Invalid address", argument);
            break;

        case ErrorCode::AddressUsed:
            SetError(error_code, "Address used", argument);
            break;

        case ErrorCode::AddressUnavailable:
            SetError(error_code, "Address unavailable", argument);
            break;

        case ErrorCode::OutOfMemory:
            SetError(error_code, "Out of memory", argument);
            break;

        case ErrorCode::DestinationAddressRequired:
            SetError(error_code, "Destination address required", argument);
            break;

        case ErrorCode::BufferFull:
            SetError(error_code, "Buffer full", argument);
            break;

        /// Process errors.
        case ErrorCode::ProcessNotFound:
            SetError(error_code, "Process not found", argument);
            break;

        case ErrorCode::ChildProcessNotFound:
            SetError(error_code, "Child process not found", argument);
            break;

        case ErrorCode::DeadlockWarning:
            SetError(error_code, "Deadlock warning", argument);
            break;

        /// Messaging errors.
        case ErrorCode::MessageBad:
            SetError(error_code, "Message bad", argument);
            break;

        case ErrorCode::MessageInvalidSize:
            SetError(error_code, "Message invalid size", argument);
            break;

        case ErrorCode::MessageUnavailable:
            SetError(error_code, "Message unavailable", argument);
            break;

        case ErrorCode::MessageInvalid:
            SetError(error_code, "Message invalid", argument);
            break;

        case ErrorCode::ProtocolUnavailable:
            SetError(error_code, "Protocol unavailable", argument);
            break;

        /// Connection and network errors.
        case ErrorCode::ConnectionAborted:
            SetError(error_code, "Connection aborted", argument);
            break;

        case ErrorCode::ConnectionRefused:
            SetError(error_code, "Connection refused", argument);
            break;

        case ErrorCode::ConnectionReset:
            SetError(error_code, "Connection reset", argument);
            break;

        case ErrorCode::HostUnreachable:
            SetError(error_code, "Host unreachable", argument);
            break;

        case ErrorCode::SocketConnected:
            SetError(error_code, "Socket connected", argument);
            break;

        case ErrorCode::SocketNotConnected:
            SetError(error_code, "Socket not connected", argument);
            break;

        /// Custom generic errors.
        case ErrorCode::NotImplemented:
            SetError(error_code, "Not implemented", argument);
            break;

        case ErrorCode::Unexpected:
            SetError(error_code, "Unexpected", argument);
            break;

        case ErrorCode::Syntax:
            SetError(error_code, "Syntax", argument);
            break;

        case ErrorCode::Bad:
            SetError(error_code, "Bad", argument);
            break;

        case ErrorCode::Aborted:
            SetError(error_code, "Aborted", argument);
            break;

        case ErrorCode::Exists:
            SetError(error_code, "Exists", argument);
            break;

        case ErrorCode::Missing:
            SetError(error_code, "Missing", argument);
            break;

        case ErrorCode::TooSmall:
            SetError(error_code, "Too small", argument);
            break;

        case ErrorCode::TooLarge:
            SetError(error_code, "Too large", argument);
            break;

        /// Custom errors for argument parsing.
        case ErrorCode::NotEnoughArguments:
            SetError(error_code, "Not enough arguments", argument);
            break;

        case ErrorCode::TooManyArguments:
            SetError(error_code, "Too many arguments", argument);
            break;

        case ErrorCode::DuplicateArgument:
            SetError(error_code, "Duplicate argument", argument);
            break;

        case ErrorCode::UnexpectedArgument:
            SetError(error_code, "Unexpected argument", argument);
            break;

        case ErrorCode::MissingArgument:
            SetError(error_code, "Missing argument", argument);
            break;

        case ErrorCode::IncompatibleArgument:
            SetError(error_code, "Incompatible argument", argument);
            break;

        /// Custom file or hardware errors.
        case ErrorCode::IsFile:
            SetError(error_code, "Is file", argument);
            break;

        case ErrorCode::NoFile:
            SetError(error_code, "No file specified", argument);
            break;

        case ErrorCode::FileEmpty:
            SetError(error_code, "File empty", argument);
            break;

        case ErrorCode::FileTooSmall:
            SetError(error_code, "File too small", argument);
            break;

        case ErrorCode::FilenameInvalid:
            SetError(error_code, "Filename invalid", argument);
            break;

        case ErrorCode::FileNotFound:
            SetError(error_code, "File not found", argument);
            break;

        case ErrorCode::FileRead:
            SetError(error_code, "File read error", argument);
            break;

        case ErrorCode::FileWrite:
            SetError(error_code, "File write error", argument);
            break;

        case ErrorCode::FileOpen:
            SetError(error_code, "File open error", argument);
            break;

        case ErrorCode::FileClose:
            SetError(error_code, "File close error", argument);
            break;

        case ErrorCode::FileChangeSize:
            SetError(error_code, "Change file size error", argument);
            break;

        case ErrorCode::FileReadOutOfMemory:
            SetError(error_code, "Out of memory while reading file", argument);
            break;

        case ErrorCode::DirectoryExists:
            SetError(error_code, "Directory exists", argument);
            break;

        case ErrorCode::DirectoryMissing:
            SetError(error_code, "Directory missing", argument);
            break;

        case ErrorCode::DirectoryNotFound:
            SetError(error_code, "Directory not found", argument);
            break;

        /// Custom memory errors.
        case ErrorCode::SourceAddressRequired:
            SetError(error_code, "Source address required", argument);
            break;

        case ErrorCode::None:
            m_error_code = ErrorCode::None;
            m_error_message.Clear();
            break;

        default:
            SetError(error_code, "Unknown", argument);
            break;
        }
    }

// Data
private:
    ErrorCode m_error_code;       /// Last error code.
    char_type const* m_separator; /// Separator string between message and argument.
    string_type m_error_message;  /// Buffer for last error_code message.
};

} // namespace ocl

#endif // OCL_GUARD_TOOLS_ERROR_HPP
